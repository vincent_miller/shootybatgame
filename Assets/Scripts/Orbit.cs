﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (12 * Mathf.Sin (Time.time), 12 * Mathf.Cos (Time.time), -1);
	}

	void OnTriggerEnter2D (Collider2D other) {
		transform.localScale *= 0.95f;
		Destroy (other.gameObject);

		if (transform.localScale.x < 1) {
			Destroy (gameObject);
			Application.LoadLevel("jam");
		}
	}
}
