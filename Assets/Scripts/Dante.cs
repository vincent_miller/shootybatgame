﻿using UnityEngine;
using System.Collections;

public class Dante : MonoBehaviour {
	private GameObject leg;
	private string num;
	private float x;

	// Use this for initialization
	void Start () {
		leg = transform.FindChild ("leg").gameObject;

		if (name == "stan") {
			x = -1.0f;
			num = "2";
		} else {
			x = 1.0f;
			num = "1";
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 vel = GetComponent<Rigidbody2D> ().velocity;
		vel.x = 0;
		GetComponent<Rigidbody2D> ().velocity = vel;
		transform.position += new Vector3 (Input.GetAxis ("Horizontal" + num + "Alt") * Time.deltaTime * 5, 0);

		if (Input.GetButtonDown ("Shoot" + num)) {
			leg.GetComponent<Rigidbody2D> ().AddTorque(1000 * x);
		}

		if ((num == "1" && Input.GetKeyDown (KeyCode.W)) || (num == "2" && Input.GetKeyDown(KeyCode.UpArrow))) {
			GetComponent<Rigidbody2D>().velocity += Vector2.up * 20;
		}
	}
}
