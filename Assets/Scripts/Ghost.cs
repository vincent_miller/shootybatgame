﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour {
	Vector3 start;
	// Use this for initialization
	void Start () {
		start = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = start + new Vector3 (20 * Mathf.Sin (Time.time * 0.1f), 10 * Mathf.Cos(Time.time));
	}
}
