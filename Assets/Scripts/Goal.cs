﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {
	private int slams = 0;
	public AudioClip wow;
	public bool side;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		GUI.color = Color.red;
		GUI.skin.label.fontSize = 26;
		if (!side) {
			GUI.Label (new Rect (50, 50, 400, 400), "Slam Dunks: " + slams);
		} else {
			GUI.Label (new Rect (Screen.width - 300, 50, 400, 400), "Slam Dunks: " + slams);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "hm") {
			other.transform.position = new Vector3(0, 3);
			other.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			slams ++;
			AudioSource.PlayClipAtPoint(wow, Vector3.zero);
		}
	}
}
