﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public AudioClip hit;

	void OnCollisionEnter2D(Collision2D other) {
		PlayerControls p = other.transform.GetComponentInParent<PlayerControls> ();
		if (p != null) {
			AudioSource.PlayClipAtPoint(hit, Vector3.zero);
			p.GetComponentInChildren<Camera> ().transform.parent = null;
			GameObject grave = Instantiate (p.deadMarker);
			grave.transform.position = p.transform.position;

			if (p.team == Team.RED) {
				Controller.RespawnRed ();
			} else {
				Controller.RespawnBlue ();
			}

			Destroy (p.gameObject);
		}
		Destroy (gameObject);
	}
}
