﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public Texture2D menuScreen;
	public GUISkin skin;

	private int tutorial = -1;
	public Texture2D[] slides;

	public const bool full = true;
	
	// Use this for initialization
	void Start () {
	}

	void OnGUI() {
		if (tutorial == -1) {
			GUI.skin = skin;
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), menuScreen);

			if (GUI.Button (new Rect (Screen.width / 2 - 300, Screen.height / 2, 600, 150), "Split Screen")) {
				Application.LoadLevel ("scene3");
			}
			
			if (GUI.Button (new Rect (Screen.width / 2 - 300, Screen.height / 2 + 100, 600, 150), "Tutorial")) {
				tutorial = 0;
			}

			if (full) {
				if (GUI.Button (new Rect (Screen.width / 2 - 300, Screen.height / 2 + 200, 600, 150), "Spooky (18+ only)")) {
					Application.LoadLevel ("spooky");
				}
			}
		} else {
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), slides[tutorial]);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if ((Input.GetMouseButtonDown (0) || Input.GetKeyDown(KeyCode.Space)) && tutorial > -1) {
			if (tutorial < slides.Length - 1) {
				tutorial ++;
			} else {
				tutorial = -1;
			}
		}
	}
}
