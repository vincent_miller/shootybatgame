﻿using UnityEngine;
using System.Collections;

public class LevelSwitch : MonoBehaviour {
	public string target;

	// Use this for initialization
	void OnTriggerEnter2D(Collider2D other) {
		if (other.GetComponentInParent<PlayerControls> () != null) {
			Application.LoadLevel (target);
		}
	}
}
