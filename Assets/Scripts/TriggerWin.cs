﻿using UnityEngine;
using System.Collections;

public class TriggerWin : MonoBehaviour {
	public Team winTeam;
	public Texture2D winScreen;
	private bool win = false;
	public Font font;
	public AudioClip winSound;
	private static int redScore = 0;
	private static int blueScore = 0;

	public string winLevel;

	// Use this for initialization
	void Update () {
		if (win && Input.GetButtonDown("Advance")) {
			win = false;
			Time.timeScale = 1.0f;
			Application.LoadLevel(winLevel);
		}
	}

	void OnGUI() {
		if (win) {
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), winScreen);
		} else {
			GUI.skin.label.fontSize = 24;
			if (winTeam == Team.RED) {
				GUI.color = Color.red;
				GUI.Label (new Rect(50, 50, 200, 200), "Score: " + redScore);
			} else {
				GUI.color = Color.blue;
				GUI.Label (new Rect(Screen.width - 150, 50, 200, 200), "Score: " + blueScore);
			}
		}
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other) {
		PlayerControls p = other.transform.GetComponentInParent<PlayerControls> ();
		if (p != null && p.team == winTeam) {
			Time.timeScale = 0.0f;
			win = true;
			if (winTeam == Team.RED) {
				redScore++;
			} else {
				blueScore++;
			}
			AudioSource.PlayClipAtPoint(winSound, Vector3.zero);
		}
	}
}
