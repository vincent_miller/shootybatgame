﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
	public Texture2D divider;
	public GameObject bluePlayer;
	public GameObject redPlayer;

	private static ExpirationTimer redRespawn;
	private static ExpirationTimer blueRespawn;
	public float respawnTime = 5.0f;

	private static bool redDead = false;
	private static bool blueDead = false;

	private Vector3 redStart;
	private Vector3 blueStart;

	public Texture2D pauseScreen;
	public GUISkin skin;

	public static bool paused = false;

	// Use this for initialization
	void Start () {
		redDead = false;
		blueDead = false;

		redRespawn = new ExpirationTimer (respawnTime);
		blueRespawn = new ExpirationTimer (respawnTime);

		redStart = GameObject.Find (redPlayer.name).transform.position;
		blueStart = GameObject.Find (bluePlayer.name).transform.position;

		paused = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (redDead && redRespawn.Expired) {
			redDead = false;
			Destroy (GameObject.Find ("RedCamera"));
			Instantiate (redPlayer).transform.position = redStart;
		}

		if (blueDead && blueRespawn.Expired) {
			blueDead = false;
			Destroy (GameObject.Find ("BlueCamera"));
			Instantiate (bluePlayer).transform.position = blueStart;
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (paused) {
				paused = false;
				Time.timeScale = 1.0f;
			} else {
				paused = true;
				Time.timeScale = 0.0f;
			}
		}
	}

	public static void RespawnRed() {
		redDead = true;
		redRespawn.Set ();
	}

	public static void RespawnBlue() {
		blueDead = true;
		blueRespawn.Set ();
	}

	void OnGUI() {
		GUI.skin = skin;
		if (Time.timeScale > 0) {
			GUI.DrawTexture (new Rect (Screen.width / 2 - 10, 0, 20, Screen.height), divider);
		}

		if (paused) {
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), pauseScreen);

			if (GUI.Button (new Rect (Screen.width / 2 - 300, Screen.height / 2, 600, 150), "Main Menu")) {
				Time.timeScale = 1.0f;
				Application.LoadLevel ("menu");
			}

			if (GUI.Button (new Rect (Screen.width / 2 - 300, Screen.height / 2 + 100, 600, 150), "Exit Game")) {
				Application.Quit();
			}
		}

		if (redDead) {
			GUI.color = Color.white;
			GUI.skin.label.fontSize = 24;
			GUI.Label(new Rect(Screen.width / 4, Screen.height / 2, 400, 400), "Respawning in: " + Mathf.CeilToInt(redRespawn.Remaining()));
		}

		if (blueDead) {
			GUI.color = Color.white;
			GUI.skin.label.fontSize = 24;
			GUI.Label(new Rect(Screen.width * 3 / 4, Screen.height / 2, 400, 400), "Respawning in: " + Mathf.CeilToInt(blueRespawn.Remaining()));
		}
	}
}
