﻿using UnityEngine;
using System.Collections;

public class Clicker : MonoBehaviour {
	public static int score = 0;
	public Sprite alt;
	private Sprite normal;
	public AudioClip click;
	public AudioClip altClick;
	public GameObject coin;
	public GameObject hm;

	private SpriteRenderer eyes;
	private SpriteRenderer glasses;
	private SpriteRenderer lasers;
	private SpriteRenderer background;

	public AudioClip sins;


	// Use this for initialization
	void Start () {
		normal = GetComponent<SpriteRenderer> ().sprite;
		eyes = transform.FindChild ("eyes").GetComponent<SpriteRenderer> ();
		glasses = transform.FindChild ("dealwithit").GetComponent<SpriteRenderer> ();
		lasers = transform.FindChild ("lasers").GetComponent<SpriteRenderer> ();
		background = GameObject.Find ("ClickerBackground").GetComponent<SpriteRenderer> ();

		eyes.enabled = false;
		glasses.enabled = false;
		lasers.enabled = false;
		background.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Return)) {
			OnMouseDown();
		}

		if (Input.GetKeyUp (KeyCode.Return)) {
			OnMouseUp ();
		}
	}

	void OnGUI() {
		GUI.skin.label.fontSize = 36;
		GUI.Label (new Rect (50, 50, 400, 400), "Score: " + score);
	}

	void OnMouseDown() {
		AudioSource.PlayClipAtPoint (click, Vector3.zero);
		score++;
		GetComponent<SpriteRenderer> ().sprite = alt;
		transform.position = new Vector3(-0.5f, -1, 0);
		transform.up = new Vector3(-0.5f, 2, 0);

		GameObject newCoin = Instantiate (coin);
		newCoin.transform.position = new Vector3 (-2, 2);
		newCoin.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-3, 5);

		if (score == 100) {
			background.enabled = true;
			Camera.main.orthographicSize = 10;
		} 

		if (score == 200) {
			lasers.enabled = true;
		}

		if (score == 300) {
			click = altClick;
		}

		if (score == 400) {
			AudioSource s = GetComponent<AudioSource> ();
			s.Stop ();
			s.clip = sins;
			s.Play ();
		}

		if (score == 500) {
			Instantiate (hm);
			Camera.main.orthographicSize = 15;
		}
	}

	void OnMouseUp() {
		GetComponent<SpriteRenderer> ().sprite = normal;
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
	}
}
