﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {
	public Team team;
	public GameObject bullet;
	public GameObject deadMarker;
	public Texture2D white;
	public AudioClip gunshot;
	public AudioClip lightClick;

	private string haxis;
	private string vaxis;
	private string fire;
	private string lightKey;
	private Vector2 direction = new Vector2 (0, 1);
	private Transform body;
	private bool lightOn = true;
	private float blinded = 0.0f;

	private PlayerControls getOtherPlayer() {
		GameObject other;
		if (team == Team.RED) {
			other = GameObject.FindGameObjectWithTag ("BluePlayer");
		} else {
			other = GameObject.FindGameObjectWithTag ("RedPlayer");
		}
		if (other != null) {
			return other.GetComponent<PlayerControls> ();
		} else {
			return null;
		}
	}

	// Use this for initialization
	void Start () {
		if (team == Team.RED) {
			haxis = "Horizontal1";
			vaxis = "Vertical1";
			fire = "Shoot1";
			lightKey = "Light1";
		} else {
			haxis = "Horizontal2";
			vaxis = "Vertical2";
			fire = "Shoot2";
			lightKey = "Light2";
		}

		body = transform.FindChild ("Body");
	}

	void OnGUI() {
		GUI.color = new Color(1.0f, 1.0f, 1.0f, blinded);
		GUI.DrawTexture(new Rect(team == Team.RED ? 0 : Screen.width / 2, 0, Screen.width / 2, Screen.height), white);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector2 input = new Vector2 (Mathf.Clamp(Input.GetAxis (haxis) + Input.GetAxis(haxis + "Alt"), -1, 1), Mathf.Clamp(Input.GetAxis (vaxis) + Input.GetAxis(vaxis + "Alt"), -1, 1));
		GetComponent<Rigidbody2D>().MovePosition(transform.position + new Vector3(input.x, input.y) * Time.fixedDeltaTime * 10);
		if (input.sqrMagnitude > 0.25f * 0.25f) {
			direction = input.normalized;
			body.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2 (-direction.x, direction.y)));
		}
	}

	void Update () {


		if (Input.GetButtonDown(fire)) {
			AudioSource.PlayClipAtPoint(gunshot, Vector3.zero);
			GameObject newbullet = Instantiate (bullet);
			newbullet.transform.position = body.TransformPoint(new Vector3(0.5f, 2));
			newbullet.transform.up = (Vector3) direction;
			Rigidbody2D rbody = newbullet.GetComponent<Rigidbody2D> ();
		    rbody.velocity = direction * 30;
		}

		if (Input.GetButtonDown(lightKey)) {
			lightOn = !lightOn;
			AudioSource.PlayClipAtPoint(lightClick, Vector3.zero);
		}

		Vector3 fpos = body.TransformPoint (new Vector3 (-0.6f, 0.5f));
		PlayerControls other = getOtherPlayer ();

		if (other != null) {
			if (lightOn) {
				Vector3 toOther = other.transform.position - transform.position;
				float dist = toOther.magnitude;
				Vector3 norm = toOther.normalized;
				float dot = Mathf.Max (Vector3.Dot (norm, direction), 0.0f) * Mathf.Max (Vector3.Dot (direction, -other.direction), 0);
				float f = Mathf.Clamp01 (Mathf.Pow (0.1f * dist, 2) / Mathf.Pow (dot, 5));
				other.blinded = 1.0f - f;
			} else {
				other.blinded = 0.0f;
			}
		} else {
			blinded = 0.0f;
		}

		GameObject darknesss = GameObject.FindWithTag("Darkness");
		Vector3 off = fpos - darknesss.transform.position;
		off += darknesss.transform.localScale / 4.0f;
		off.x /= darknesss.transform.localScale.x / 2.0f;
		off.y /= darknesss.transform.localScale.y / 2.0f;
		
		if (team == Team.RED) {
			Shader.SetGlobalVector ("Player1Light", new Vector4 (off.x, off.y, 0, 0));
			Shader.SetGlobalVector ("Player1Dir", new Vector4 (direction.x, direction.y, 0, 0));
			Shader.SetGlobalInt ("Player1LightOn", lightOn ? 1 : 0);
		} else {
			Shader.SetGlobalVector ("Player2Light", new Vector4 (off.x, off.y, 0, 0));
			Shader.SetGlobalVector ("Player2Dir", new Vector4 (direction.x, direction.y, 0, 0));
			Shader.SetGlobalInt ("Player2LightOn", lightOn ? 1 : 0);
		}
	}
}
