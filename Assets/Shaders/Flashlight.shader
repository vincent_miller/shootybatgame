﻿Shader "Custom/Flashlight" {
	Properties {
	}
	
	SubShader {
		
		Tags {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
		
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"
			
			float4 Player1Light;
			float4 Player1Dir;
			int Player1LightOn;
			
			float4 Player2Light;
			float4 Player2Dir;
			int Player2LightOn;
			
			struct v2f {
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
			};
			
			v2f vert (appdata_base v) {
                v2f o;
                o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord;
                return o;
            }
            
            float inLight(bool lon, float2 uv, float4 pos, float4 dir) {
            	if (lon == 0) {
            		return 1.0f;
        		}
        		
            	if (pos.x < 0 || pos.x > 1 || pos.y < 0 || pos.y > 1) {
            		return 1.0f;
            	}
            	
            	float d = distance(uv, pos.xy);
                
                float2 l = normalize(uv - pos.xy);
                float dotp = dot(l, dir.xy);
                
                /*
                
                if (dotp > 0.85 && d < 0.05) {
                	return 0;
            	} else if (d < 0.05) {
            		return min((0.85 - dotp) * 5, 1.0);
            	} else if (dotp > 0.85) { 
            		return min((d - 0.05) * 100, 1.0);
        		} else {
        			return max(min((0.85 - dotp) * 5, 1.0), min((d - 0.05) * 100, 1.0));
    			}*/
    			
    			if (dotp < 0) {
    				dotp = 0;
				}
    			
    			return clamp(min(10 * d / pow(dotp, 5), 100 * d), 0, 1);
            }

            fixed4 frag (v2f i) : SV_Target {
            	float a = inLight(Player1LightOn, i.uv, Player1Light, Player1Dir);
            	float b = inLight(Player2LightOn, i.uv, Player2Light, Player2Dir);
           		
            	float f = 1 - clamp((1 - a) + (1 - b), 0, 1);
        		return fixed4(0, 0, 0, f);
            }
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
